# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/tamara/Desktop/azrs-tracking/build/server/server/ServerApplication_autogen/EWIEGA46WW/qrc_resources.cpp" "/home/tamara/Desktop/azrs-tracking/build/server/server/CMakeFiles/ServerApplication.dir/ServerApplication_autogen/EWIEGA46WW/qrc_resources.cpp.o"
  "/home/tamara/Desktop/azrs-tracking/build/server/server/ServerApplication_autogen/mocs_compilation.cpp" "/home/tamara/Desktop/azrs-tracking/build/server/server/CMakeFiles/ServerApplication.dir/ServerApplication_autogen/mocs_compilation.cpp.o"
  "/home/tamara/Desktop/azrs-tracking/server/server/lobby.cpp" "/home/tamara/Desktop/azrs-tracking/build/server/server/CMakeFiles/ServerApplication.dir/lobby.cpp.o"
  "/home/tamara/Desktop/azrs-tracking/server/server/main.cpp" "/home/tamara/Desktop/azrs-tracking/build/server/server/CMakeFiles/ServerApplication.dir/main.cpp.o"
  "/home/tamara/Desktop/azrs-tracking/server/server/player.cpp" "/home/tamara/Desktop/azrs-tracking/build/server/server/CMakeFiles/ServerApplication.dir/player.cpp.o"
  "/home/tamara/Desktop/azrs-tracking/server/server/server.cpp" "/home/tamara/Desktop/azrs-tracking/build/server/server/CMakeFiles/ServerApplication.dir/server.cpp.o"
  "/home/tamara/Desktop/azrs-tracking/server/server/session.cpp" "/home/tamara/Desktop/azrs-tracking/build/server/server/CMakeFiles/ServerApplication.dir/session.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NETWORK_LIB"
  "QT_NO_DEBUG"
  "QT_WIDGETS_LIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "server/server"
  "../server/server"
  "server/server/ServerApplication_autogen/include"
  "/usr/include/x86_64-linux-gnu/qt5"
  "/usr/include/x86_64-linux-gnu/qt5/QtCore"
  "/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++"
  "/usr/include/x86_64-linux-gnu/qt5/QtWidgets"
  "/usr/include/x86_64-linux-gnu/qt5/QtGui"
  "/usr/include/x86_64-linux-gnu/qt5/QtNetwork"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
